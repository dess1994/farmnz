
import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
 paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    background: 'rgba(255, 255, 255, 0.5)',
    //opacity:0.2,
    minHeight:"100vh",
  },
  avatar: {
    marginTop: "30px",
    height:"100px",
    marginLeft:"auto",
    marginRight:"auto",
  },
  miniSuper:{
    marginLeft:"auto",
    marginRight:"auto",
  },
  welcomeText:{
    color:"#fff",
    opacity:0.8,
    fontWeight:"100",
    fontStyle:"normal",
    fontSize:"30px",
    letterSpacing:"2px",
  },
  super:{
    textAlign:"center",
    display:"flex",
    minHeight:"100vh",
  },
  form: {
    width: '80%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    marginRight:"auto",
    marginLeft:"auto",
  },
  submit: {
    backgroundColor:"#1ebf73",
    color:"#fff",
    height:"45px",
    textTransform:"none",
  },
  submitGoogle: {
    backgroundColor:"inherit",
    color:"grey",
    width:"80%",
    marginTop:"20px",
    marginBottom:"20px",
    height:"45px",
    textTransform:"none",
  },
  containerForm:{
    backgroundColor:"#fff",
    width:"35vw",
    marginBottom:"40px",
  },
  bigContainer:{
    maxWidth:"44vw",
  },
  dividerPass:{
    marginTop:"20px"
  },
  forgotPass:{
    marginTop:"10px",
  },
  signUp:{
    marginTop:"20px",
    paddingBottom:"20px",
  },
  inpLog:{
    '& .MuiOutlinedInput-root':{
      height:"45px",
    },
  },
}));

export default function Login() {
  const classes = useStyles();

  return (
    <Container component="main"  className={classes.bigContainer} >
      <CssBaseline />
      <Container >
       <div className={classes.paper} >
        <div className={classes.super} >
          <div >
            <img src="./Images/Logo.png" className={classes.avatar}/>
            <h1 className={classes.welcomeText}>¡Welcome!</h1>
            <div className={classes.containerForm}>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                className={classes.submitGoogle}
              > 
                <img src="./Images/gIcon.png" height="17px" style={{marginTop:"1px"}} /> 
                <span style={{marginLeft:"5px"}}>Log in with</span> 
                <span style={{color:"red",fontWeight:"700", marginLeft:"5px"}}> Google</span>
              </Button>
              <Divider component="form"/>

              <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                name="email"
                placeholder="Email address"
                autoFocus
                className={classes.inpLog}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                type="password"
                id="password"
                placeholder="Password"
                className={classes.inpLog}
              />
              <div>
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"/>
                <h5>Show your password</h5>
              </div>
              
              <Button
                type="submit"
                fullWidth
                variant="contained"
                className={classes.submit}
              >
                <a href="/Dashboard"  style={{textDecoration:'none', color:"#fff"}}> Log in
                </a>
              </Button>
              <div container>
                <div className={classes.forgotPass}>
              
                   <a href="/notPassword" style={{textDecoration:'none', color:"red"}}> Forgot your password?</a>
                  
                </div>
                 <Divider variant="middle" className={classes.dividerPass} />
                <div className={classes.signUp}>
                  <a href="#" style={{textDecoration:'none', color:"grey"}}>
                    Don't have an account? <span style={{color:"#1ebf73",fontWeight:"700"}} > Sign Up </span>
                  </a>
                </div>
              </div>
            </form>
            </div>
          </div>
        </div> 
        </div>
      </Container>
    </Container>
  );
}