import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Login from './Pages/Login/Login';
import Dashboard from './Pages/Dashboard/Dashboard';

const Routes = () =>{
	return(
		<Switch>
			<Route exact path='/' component={Login} />
			<Route path='/Dashboard' component={Dashboard} />

		</Switch>
		)
}

export default Routes;